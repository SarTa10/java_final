package com.example.finalexam.controllers;

import com.example.finalexam.dto.ApiResponse;
import com.example.finalexam.dto.CityDto;
import com.example.finalexam.services.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CityController {
    public final CityService cityService;

    @Autowired
    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @PostMapping("/city/add")
    public ApiResponse addCity(@RequestBody CityDto cityDto) {
        return cityService.addCity(cityDto);
    }

    @PostMapping("/hotel/get/all")
    public ApiResponse getAllCities() {
        return cityService.getAllCities();
    }
}
