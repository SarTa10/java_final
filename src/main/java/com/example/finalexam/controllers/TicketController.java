package com.example.finalexam.controllers;

import com.example.finalexam.dto.ApiResponse;
import com.example.finalexam.dto.TicketDto;
import com.example.finalexam.services.TicketsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TicketController {
    private final TicketsService ticketsService;

    @Autowired
    public TicketController(TicketsService ticketsService) {
        this.ticketsService = ticketsService;
    }

    @PostMapping("/tickets/add")
    public ApiResponse addTicket(@RequestBody TicketDto ticketDto) {
        return ticketsService.addTicket(ticketDto);
    }
}
