package com.example.finalexam.controllers;

import com.example.finalexam.dto.ApiResponse;
import com.example.finalexam.dto.TravelDto;
import com.example.finalexam.entities.Travel;
import com.example.finalexam.services.TravelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TravelController {

    private final TravelService travelService;

    @Autowired
    public TravelController(TravelService travelService) {
        this.travelService = travelService;
    }

    @PostMapping("/travel/add")
    public ApiResponse addTravel(@RequestBody TravelDto travelDto) {
        return travelService.addTravel(travelDto);
    }

    @GetMapping("/travel/get/all")
    public ApiResponse getAllTravels() {
        return  travelService.getAllTravel();
    }

    @GetMapping("/travel/get/stats")
    public ApiResponse getTravelStatistics() {
        return travelService.getAllTravelsFromThisMonth();
    }

}
