package com.example.finalexam.dto;

import lombok.Data;

import java.util.List;

@Data
public class CityDto {
    private Long cityId;
    private String CityName;
    private List<Long> cityIdList;
}
