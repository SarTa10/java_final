package com.example.finalexam.dto;

import lombok.Data;

@Data
public class TicketDto {
    private Long ticketId;
    private Double cost;
    private Integer seat;
    private Long CityId;
}
