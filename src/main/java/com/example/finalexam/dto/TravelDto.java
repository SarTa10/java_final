package com.example.finalexam.dto;

import lombok.Data;

import java.util.Date;

@Data
public class TravelDto {
    private Long travelId;
    private Long ticketId;
    private Long flight;
    private Date flightStartDate;
    private Date flightEndDate;
    private String travelerName;
}
