package com.example.finalexam.dto;

public interface TravelStatisticsDto {
    String GetCityName();
    Integer getSoldTickets();
    String getTotalAmount();
}
