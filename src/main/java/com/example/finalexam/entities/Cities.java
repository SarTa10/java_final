package com.example.finalexam.entities;

import com.example.finalexam.dto.CityDto;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "CITIES")
public class Cities extends AppEntity{
    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "cityId", sequenceName = "CITY_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cityId")
    private Long id;

    @Column(name="CITY_NAME")
    private String name;

    public Cities(CityDto cityDto) {
        this.name = cityDto.getCityName();
    }
}
