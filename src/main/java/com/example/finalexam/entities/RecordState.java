package com.example.finalexam.entities;

public enum RecordState {
    ACTIVE, INACTIVE, DELETED
}
