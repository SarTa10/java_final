package com.example.finalexam.entities;

import com.example.finalexam.dto.TicketDto;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "TICKETS")
public class Tickets extends AppEntity {
    @Id
    @Column(name="ID")
    @SequenceGenerator(name="ticketIdSeq", sequenceName = "TICKET_ID_SEQ",allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ticketIdSeq")
    private Long id;

    @Column(name="COST")
    private Double cost;

    @Column(name="SEAT")
    private  Integer seat;

    @ManyToOne
    private Cities city;

    public Tickets(TicketDto ticketDto) {
        this.cost=ticketDto.getCost();
        this.seat=ticketDto.getSeat();
    }
}
