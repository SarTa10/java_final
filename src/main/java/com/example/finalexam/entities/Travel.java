package com.example.finalexam.entities;

import com.example.finalexam.dto.TravelDto;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name="TRAVEL")
public class Travel extends AppEntity{

    @Id
    @Column(name="ID")
    @SequenceGenerator(name="travelIdSeq", sequenceName = "TRAVEL_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "travelIdSeq")
    private Long id;

    @ManyToOne
    private Tickets ticket;

    @Column(name="FLIGHT", nullable = false)
    private Long flight;

    //date when u can buy tickets
    @Column(name="FLIGHT_START", nullable = false)
    private Date flightStart;

    //date when u can't buy tickets any more
    @Column(name="FLIGHT_END", nullable = false)
    private Date flightEnd;

    @Column(name="TRAVELER_NAME", nullable = false)
    private String travelerName;

    public Travel(TravelDto travelDto) {
        this.flightStart = travelDto.getFlightStartDate();
        this.flightEnd = travelDto.getFlightEndDate();
        this.flight = travelDto.getFlight();
        this.travelerName = travelDto.getTravelerName();
    }
}
