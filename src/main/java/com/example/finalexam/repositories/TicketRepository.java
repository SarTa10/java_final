package com.example.finalexam.repositories;

import com.example.finalexam.entities.Tickets;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TicketRepository extends JpaRepository<Tickets, Long>, JpaSpecificationExecutor<Tickets> {
    @Query("SELECT t from Tickets t where t.id not in " +
    "(SELECT t from Tickets t inner join Travel tr on tr.ticket.id = t.id where current_date between tr.flightStart and tr.flightEnd)")
    List<Tickets> findAllFreeTickets();

    @Query("SELECT t from Tickets t inner join Travel tr on tr.ticket.id = t.id where current_date between tr.flightStart and tr.flightEnd")
    List <Tickets> findAllSoldTickets();

    @Query("SELECT t from Tickets t where t.city.id in :cityIdList and t.id not in " +
    "(Select t from Tickets t inner join Travel tr on tr.ticket.id = t.id where current_date between tr.flightStart and tr.flightEnd)")
    List<Tickets> findAllNotSoldTicketIds(@Param("cityIdList") List<Long> cityIdList);
}
