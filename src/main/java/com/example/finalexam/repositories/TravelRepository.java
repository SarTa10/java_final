package com.example.finalexam.repositories;

import com.example.finalexam.dto.TravelStatisticsDto;
import com.example.finalexam.entities.Travel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TravelRepository extends JpaRepository<Travel, Long>, JpaSpecificationExecutor<Travel> {
    @Query(value = "select c.name as cityName, count(distinct ticket_id) as soldTickets, sum(cost) as totalAmount from Travel t \n" +
            "inner join tickets tc on t.ticket_id = tc.id \n" +
            "inner join cities c on tc.city_id = c.id\n"+
            "where month(t.end_date) = month(current_date) \n" +
            "group by city_id", nativeQuery = true)
    List<TravelStatisticsDto> findTravelStatistics();

}
