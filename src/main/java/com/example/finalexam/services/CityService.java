package com.example.finalexam.services;

import com.example.finalexam.apiUtils.ApiUtils;
import com.example.finalexam.apiUtils.IncorrectParameterException;
import com.example.finalexam.dto.ApiResponse;
import com.example.finalexam.dto.CityDto;
import com.example.finalexam.entities.Cities;
import com.example.finalexam.repositories.CitiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityService {
    private final CitiesRepository citiesRepository;

    @Autowired
    public CityService(CitiesRepository citiesRepository) {
        this.citiesRepository = citiesRepository;
    }

    public ApiResponse addCity(CityDto cityDto) {
        if(cityDto == null || cityDto.getCityName() == null || cityDto.getCityName().isEmpty()) {
            throw new IncorrectParameterException().addIncorrectParameter("city");
        }

        Cities city = citiesRepository.save(new Cities(cityDto));
        return ApiUtils.getApiResponse(city);
    }

    public ApiResponse getAllCities() {
        List<Cities> cities = citiesRepository.findAll();

        return new ApiResponse().addData("cities", cities);
    }
}
