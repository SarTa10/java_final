package com.example.finalexam.services;

import com.example.finalexam.apiUtils.ApiUtils;
import com.example.finalexam.apiUtils.IncorrectParameterException;
import com.example.finalexam.apiUtils.NoSuchElementFoundException;
import com.example.finalexam.dto.ApiResponse;
import com.example.finalexam.dto.TicketDto;
import com.example.finalexam.entities.Cities;
import com.example.finalexam.entities.Tickets;
import com.example.finalexam.repositories.CitiesRepository;
import com.example.finalexam.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Access;
import java.util.List;
import java.util.Optional;

@Service
public class TicketsService {
    private final CitiesRepository citiesRepository;
    private final TicketRepository ticketRepository;

    @Autowired
    public TicketsService(CitiesRepository citiesRepository, TicketRepository ticketRepository) {
        this.citiesRepository = citiesRepository;
        this.ticketRepository = ticketRepository;
    }

    public ApiResponse addTicket(TicketDto ticketDto) {
        if(ticketDto == null ||
        ticketDto.getCost() == null ||
        ticketDto.getSeat() == null
        ) {
            throw new IncorrectParameterException().addIncorrectParameter("ticket");
        }

        Optional<Cities> city = this.citiesRepository.findById(ticketDto.getTicketId());

        if(city.isEmpty()) {
            throw new NoSuchElementFoundException().addDescription("hotel", "by id");
        }

        Tickets newTicket = new Tickets(ticketDto);
        newTicket.setCity(city.get());

        Tickets ticket = ticketRepository.save(newTicket);

        return ApiUtils.getApiResponse(ticket);

    }


}
