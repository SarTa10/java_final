package com.example.finalexam.services;

import com.example.finalexam.apiUtils.ApiUtils;
import com.example.finalexam.apiUtils.IncorrectParameterException;
import com.example.finalexam.apiUtils.NoSuchElementFoundException;
import com.example.finalexam.dto.ApiResponse;
import com.example.finalexam.dto.TravelDto;
import com.example.finalexam.dto.TravelStatisticsDto;
import com.example.finalexam.entities.Tickets;
import com.example.finalexam.entities.Travel;
import com.example.finalexam.repositories.TicketRepository;
import com.example.finalexam.repositories.TravelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TravelService {
    private final TravelRepository travelRepository;
    private final TicketRepository ticketRepository;

    @Autowired
    public TravelService(TravelRepository travelRepository, TicketRepository ticketRepository) {
        this.travelRepository = travelRepository;
        this.ticketRepository = ticketRepository;
    }

    public ApiResponse addTravel(TravelDto travelDto) {
        if(travelDto == null ||
            travelDto.getTravelId() == null ||
            travelDto.getFlight() == null ||
            travelDto.getTravelerName() == null ||
            travelDto.getFlightStartDate()  == null ||
            travelDto.getFlightEndDate() ==null) {
            throw new IncorrectParameterException().addIncorrectParameter("travel");
        }

        if(travelDto.getFlightStartDate().after(travelDto.getFlightEndDate())) {
            throw new IncorrectParameterException().addIncorrectParameter("travel dates");
        }

        Optional<Tickets> ticket = this.ticketRepository.findById(travelDto.getTicketId());

        if(ticket.isEmpty()) {
            throw new NoSuchElementFoundException().addDescription("room", "by id");
        }

        Travel newTravel = new Travel(travelDto);
        newTravel.setTicket(ticket.get());

        Travel travel = travelRepository.save(newTravel);
        return ApiUtils.getApiResponse(travel);
    }

    public ApiResponse getAllTravel() {
        List<Travel> travels = travelRepository.findAll();

        return new ApiResponse().addData("travels", travels);
    }

    public ApiResponse getAllTravelsFromThisMonth() {
        List<TravelStatisticsDto> travelStatistics = travelRepository.findTravelStatistics();

        return new ApiResponse().addData("travels", travelStatistics);
    }
}
