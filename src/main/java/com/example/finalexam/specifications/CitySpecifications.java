package com.example.finalexam.specifications;

import com.example.finalexam.entities.Cities;
import org.springframework.data.jpa.domain.Specification;

public class CitySpecifications {
    private CitySpecifications() {

    }

    public static Specification<Cities> idEquals(Long id) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("id"), id));
    }

    public static Specification<Cities> idNotNull() {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.isNotNull(root.get("id")));
    }
}
